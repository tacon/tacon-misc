package dev.tacon.misc.util;

import java.util.HashMap;
import java.util.Map;

public final class ClassUtils {

	private static final Map<Class<?>, Class<?>> PRIMITIVES;
	private static final Map<Class<?>, Class<?>> WRAPPERS;

	static {
		PRIMITIVES = new HashMap<>();
		PRIMITIVES.put(Boolean.class, Boolean.TYPE);
		PRIMITIVES.put(Byte.class, Byte.TYPE);
		PRIMITIVES.put(Character.class, Character.TYPE);
		PRIMITIVES.put(Double.class, Double.TYPE);
		PRIMITIVES.put(Float.class, Float.TYPE);
		PRIMITIVES.put(Integer.class, Integer.TYPE);
		PRIMITIVES.put(Long.class, Long.TYPE);
		PRIMITIVES.put(Short.class, Short.TYPE);
		PRIMITIVES.put(Void.class, Void.TYPE);

		WRAPPERS = new HashMap<>();
		WRAPPERS.put(Boolean.TYPE, Boolean.class);
		WRAPPERS.put(Byte.TYPE, Byte.class);
		WRAPPERS.put(Character.TYPE, Character.class);
		WRAPPERS.put(Double.TYPE, Double.class);
		WRAPPERS.put(Float.TYPE, Float.class);
		WRAPPERS.put(Integer.TYPE, Integer.class);
		WRAPPERS.put(Long.TYPE, Long.class);
		WRAPPERS.put(Short.TYPE, Short.class);
		WRAPPERS.put(Void.TYPE, Void.class);
	}

	public static Class<?> unwrap(final Class<?> cls) {
		return cls.isPrimitive() ? cls : PRIMITIVES.get(cls);
	}

	public static Class<?> wrap(final Class<?> cls) {
		return cls.isPrimitive() ? WRAPPERS.get(cls) : cls;
	}

	@SuppressWarnings("unchecked")
	public static <T> Class<T> uncheckedCast(final Class<?> c) {
		return (Class<T>) c;
	}

	private ClassUtils() {
		throw new AssertionError();
	}
}
