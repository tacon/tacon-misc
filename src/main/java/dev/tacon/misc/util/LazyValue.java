package dev.tacon.misc.util;

import static java.util.Objects.requireNonNull;

import java.util.function.Function;
import java.util.function.Supplier;

import dev.tacon.annotations.NonNull;

public final class LazyValue<T> implements Supplier<T> {

	private Supplier<? extends T> supplier;
	private T value;

	private LazyValue(final Supplier<? extends T> supplier) {
		this.supplier = supplier;
	}

	@SuppressWarnings("unchecked")
	public static <T> LazyValue<T> of(final @NonNull Supplier<? extends T> supplier) {
		requireNonNull(supplier);
		if (supplier instanceof LazyValue) {
			return (LazyValue<T>) supplier;
		}
		return new LazyValue<>(supplier);
	}

	@Override
	public T get() {
		return this.supplier == null ? this.value : this.computeValue();
	}

	public boolean isComputed() {
		return this.supplier == null;
	}

	public <U> LazyValue<U> map(final Function<? super T, ? extends U> mapper) {
		return of(() -> mapper.apply(this.get()));
	}

	private T computeValue() {
		if (this.supplier != null) {
			this.value = this.supplier.get();
			this.supplier = null;
		}
		return this.value;
	}
}
